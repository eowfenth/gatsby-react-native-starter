module.exports = {
  siteMetadata: {
    title: 'Gatsby React Native Default Starter',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-react-next',
    'gatsby-plugin-react-native-web',
  ],
};
