import React from 'react';
import Link from 'gatsby-link';
import { Text, View } from 'react-native';

const IndexPage = () => (
  <View>
    <Text>Hi people</Text>
    <Text>Welcome to your new Gatsby site.</Text>
    <Text>Now go build something great.</Text>
    <Link to="/page-2/">Go to page 2</Link>
  </View>
);

export default IndexPage;
