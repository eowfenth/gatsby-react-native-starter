import React from 'react';
import { Text, View } from 'react-native';

const NotFoundPage = () => (
  <View>
    <Text>NOT FOUND</Text>
    <Text>You just hit a route that doesn&#39;t exist... the sadness.</Text>
  </View>
);

export default NotFoundPage;
