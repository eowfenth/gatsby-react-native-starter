import React from 'react';
import Link from 'gatsby-link';
import { Text, View } from 'react-native';

const Header = ({ siteTitle }) => (
  <View>
    <View>
      <Text>
        <Link to="/">{siteTitle}</Link>
      </Text>
    </View>
  </View>
);

export default Header;
